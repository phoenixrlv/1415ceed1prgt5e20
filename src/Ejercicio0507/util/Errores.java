/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 * Fichero: Errores.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 08-nov-2013
 */
public class Errores {

  public String tipo(int e) {
    String t = "";
    switch (e) {
      case 1:
        t = "Opción incorrecta del menu";
        break;
      case 2:
        t = "Debe ser positivo";
        break;
      case 3:
        t = "Email incorrecto";
        break;
    }
    return t;

  }
}
