/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 * Fichero: Cliente.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-ene-2014
 */
public class Cliente {

  private String nombre;
  private int edad;
  private String email;

  public Cliente(String n, int p, String e) {
    nombre = n;
    edad = p;
    email = e;
  }

  /**
   * @return the nombre
   */
  public String getNombre() {
    return nombre;
  }

  /**
   * @param nombre the nombre to set
   */
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  /**
   * @return the edad
   */
  public int getEdad() {
    return edad;
  }

  /**
   * @param edad the edad to set
   */
  public void setEdad(int edad) {
    this.edad = edad;
  }

  /**
   * @return the email
   */
  public String getEmail() {
    return email;
  }

  /**
   * @param email the email to set
   */
  public void setEmail(String email) {
    this.email = email;
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("\nNombre: ");
    sb.append(nombre);
    sb.append("\nEdad: ");
    sb.append(edad);
    sb.append("\nEmail: ");
    sb.append(email);
    return sb.toString();
  }
}
