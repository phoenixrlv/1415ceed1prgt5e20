/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0507.vista;

import Ejercicio0507.controlador.Main;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import modelo.Cliente;
import util.Util;

/**
 * Fichero: VistaArticulo.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-ene-2014
 */
public class VistaArrayList {

  public void getClientes(ArrayList<Cliente> ClientesArrayList)
          throws IOException {

    Cliente cliente;
    Vista vista = new Vista();
    Util util = new Util();
    Main main = new Main();
    String nombre, email;
    int edad;
    int correcto;

    System.out.println("VISTA ARRAYLIST");
    System.out.println("TOMA DE DATOS");

    for (int i = 0; i < main.TALLA; i++) {

      nombre = "";
      edad = 0;
      email = "";

      System.out.print("Nombre: ");
      nombre = util.pedirString();

      while (edad <= 0) {
        System.out.print("Edad: ");
        edad = util.pedirInt();
        if (edad <= 0) {
          vista.error(2);
        }
      }

      correcto = 1;
      while (correcto == 1) {
        System.out.print("Email: ");
        email = util.pedirString();
        if (util.invalidoEmail(email) == true) {
          correcto = 0;
        } else {
          correcto = 1;
          vista.error(3);
        }
      }

      cliente = new Cliente(nombre, edad, email);

      ClientesArrayList.add(cliente);
    }
  }

  public void showArticulos(ArrayList<Cliente> ClientesArrayList) {

    System.out.println("VISTA ARRAYLIST");
    System.out.println("MOSTRANDO DATOS");
    Iterator it = ClientesArrayList.iterator();
    while (it.hasNext()) {
      Cliente articulo = (Cliente) it.next();
      System.out.println(articulo.getNombre()
              + " " + articulo.getEdad()
              + " " + articulo.getEmail());
    }

  }
}
