/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0507.controlador;

import Ejercicio0507.vista.Vista;
import java.io.IOException;
import java.util.ArrayList;
import modelo.Cliente;
import util.Util;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-ene-2014
 */
public class Main {

  final public static int TALLA = 3;

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) throws IOException {
    int ope;

    Util util = new Util();
    Vista vista = new Vista();

    ControladorArrayList controladorArrayList = new ControladorArrayList();
    ControladorArray controladorArray = new ControladorArray();

    Cliente[] ClientesArray = new Cliente[TALLA];
    ArrayList<Cliente> ClientesArrayList = null;

    do {
      ope = vista.opcionE();

      switch (ope) {
        case 0: // Fin
          vista.fin();
          break;
        case 1: // Array
          controladorArray.menu(ClientesArray);
          break;
        case 2: // ArrayList
          controladorArrayList.menu(ClientesArrayList);
          break;
        default:
          vista.error(1);
      }

    } while (ope != 0);

  }
}
