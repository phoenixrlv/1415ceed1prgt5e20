/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0507.controlador;

import Ejercicio0507.vista.Vista;
import Ejercicio0507.vista.VistaArray;
import java.io.IOException;
import modelo.Cliente;
import util.Util;

/**
 * Fichero: ControladorArray.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-ene-2014
 */
public class ControladorArray {

  public void menu(Cliente ClientesArray[]) throws IOException {

    VistaArray vistaArray = new VistaArray();
    Vista vista = new Vista();
    Util util = new Util();
    int ope;

    do {
      ope = vista.opcionO();
      switch (ope) {
        case 0: // Finnt 
          vista.fin();
          break;
        case 1: // Añadir
          vistaArray.getArticulos(ClientesArray);
          break;
        case 2:  // Listar
          vistaArray.showArticulos(ClientesArray);
          break;

        default:
          vista.error(1);
      }
    } while (ope != 0);

  }
}
