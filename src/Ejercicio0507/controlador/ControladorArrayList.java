/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio0507.controlador;

import Ejercicio0507.vista.Vista;
import Ejercicio0507.vista.VistaArrayList;
import java.io.IOException;
import java.util.ArrayList;
import modelo.Cliente;
import util.Util;

/**
 * Fichero: ControladorArray.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 14-ene-2014
 */
public class ControladorArrayList {

  public void menu(ArrayList<Cliente> ClientesArrayList)
          throws IOException {

    VistaArrayList vistaArrayList = new VistaArrayList();
    Vista vista = new Vista();
    Util util = new Util();
    int ope;

    do {
      ope = vista.opcionO();
      switch (ope) {
        case 1: // Añadir
          vistaArrayList.getClientes(ClientesArrayList);
          break;
        case 2:  // Listar
          vistaArrayList.showArticulos(ClientesArrayList);
          break;

        default:
          vista.error(1);
      }
    } while (ope != 0);

  }
}
